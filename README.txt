Tyler Berkshire and Kevin Yeboah

This program accesses the Twitter API to gather data on specific users

Run the crawler like: crawler.py <input-file-path>

In this case, it can be run using: crawler.py input.txt

Users' screen names must be listed on a separate line in the input file. Feel free to add more,
keeping in mind the 15 request Twitter API limit

The crawler then prints out the first 50 tweets containing the words "Ohio" and "weather", followed
by the first 50 tweets originating from Dayton, OH.

It then gets all of UD's friends up to 5 hops and prints out the top ten most followed and most
and most following accounts