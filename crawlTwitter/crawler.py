# crawler.py
# Tyler Berkshire and Kevin Yeboah
import sys
import const
import re
import tweepy
import time
from tweepy import Stream
from tweepy import TweepError
from tweepy.streaming import StreamListener
from colorama import Fore, Style


class TweetListener(StreamListener):
    """ Basic listener to print twitter data from the stream"""

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status_code):
        print(status_code)


def part_one():
    if len(sys.argv) != 2:
        print("Usage: crawler.py <inputfile>\n")
        exit(1)

    try:
        f = open(sys.argv[1], "r")
        screen_names = f.readlines()
    except FileNotFoundError:
        print(sys.argv[1], " does not exist\n")
        exit(1)

    auth = tweepy.OAuthHandler(const.CONSUMER_KEY, const.CONSUMER_SECRET)
    auth.set_access_token(const.ACCESS_TOKEN, const.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    # twitter_stream = Stream(auth, TweetListener())

    for user in screen_names:
        try:
            current_user = api.get_user(screen_name=user)
        except Exception:
            print(user, " is not a valid user")
            continue

        # Task 1
        print("***************************************")
        print(Fore.GREEN + "User Name:", end=" ")
        print(Style.RESET_ALL, current_user.name)
        print(Fore.GREEN + "Screen Name:", end=" ")
        print(Style.RESET_ALL, current_user.screen_name)
        print(Fore.GREEN + "User ID:", end=" ")
        print(Style.RESET_ALL, current_user.id)
        print(Fore.GREEN + "Location:", end=" ")
        print(Style.RESET_ALL, current_user.location)
        print(Fore.GREEN + "User description:", end=" ")
        print(Style.RESET_ALL, current_user.description)
        print(Fore.GREEN + "Number of Followers:", end=" ")
        print(Style.RESET_ALL, current_user.followers_count)  # Num users following this account

        # Task2 print first 20 followers and friends
        # NOTE: followers and friends methods are paged and only return first 100
        # code can be modified in the future to go through pages
        try:
            followers = api.followers(screen_name=user)
            following = api.friends(screen_name=user)
            friends = []
            first_twenty = []

            for f in followers:
                if followers.index(f) < 20:
                    first_twenty.append(f.screen_name)
                try:
                    friend = following.index(f)
                    friends.append(f.screen_name)
                except ValueError:
                    continue

            print(Fore.GREEN + "First 20 followers:")
            for f in first_twenty:
                print(Style.RESET_ALL, "\t" + f)

            print(Fore.GREEN + "Friends:")
            for f in friends:
                print(Style.RESET_ALL, "\t" + f)

        except TweepError:
            print(TweepError)

        print(Fore.GREEN + "Number of Friends:", end=" ")
        print(Style.RESET_ALL, current_user.friends_count)  # Num users this account is following
        print(Fore.GREEN + "Number of Tweets:", end=" ")
        print(Style.RESET_ALL, current_user.statuses_count)
        print(Fore.GREEN + "Url:", end=" ")
        print(Style.RESET_ALL, current_user.url)


def part_two():
    auth = tweepy.OAuthHandler(const.CONSUMER_KEY, const.CONSUMER_SECRET)
    auth.set_access_token(const.ACCESS_TOKEN, const.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    # 3.a Print the first 50 tweets containing Ohio and Weather
    fetched_tweets = [status for status in tweepy.Cursor(api.search, "Ohio AND weather").items(50)]
    for tweet in fetched_tweets:
        words = tweet.text.split(' ')
        regex = '([Oo]{1}[Hh]{1}[Ii]{1}[Oo]{1}|OH|[Ww]{1}[Ee]{1}[Aa]{1}[Tt]{1}[Hh]{1}[Ee]{1}[Rr]{1})'
        for word in words:
            if re.search(regex, word):
                print(Fore.GREEN + word, end=" ")
                print(Style.RESET_ALL)
            else:
                print(word.encode, end=" ")
        print()
        print("***************************************")

    print("************************************************************************")
    print("************************************************************************")
    print("Now printing the last 50 Tweets form the Dayton region...")
    print("************************************************************************")
    print("************************************************************************")

    # 3.b Print the first 50 tweets originating from Dayton, OH
    fetched_tweets = [status for status in tweepy.Cursor(api.search, geocode="39.758949,-84.191605,25mi").items(50)]
    print(Fore.GREEN + "Last 50 Dayton Tweets:", end="\n")
    print(Style.RESET_ALL)
    for tweet in fetched_tweets:
        print("\t" + tweet.text + "\n")

    search_results = tweepy.Cursor(api.search, geocode="39.758949,-84.191605,25mi").items(50)
    print(search_results)


def part_three():
    auth = tweepy.OAuthHandler(const.CONSUMER_KEY, const.CONSUMER_SECRET)
    auth.set_access_token(const.ACCESS_TOKEN, const.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    # 4. a) get 5 hop friends of the UD twitter account
    ud_followers = get_followers(api, "univofdayton")
    ud_following = get_following(api, "univofdayton")
    first_hop_ids = get_friends(ud_followers, ud_following)
    first_hop_friends = []  # list of tuples (id, screen_name, following_count, follower_count)
    for uid in first_hop_ids:
        u = api.get_user(uid)
        first_hop_friends.append((uid, u.screen_name, u.friends_count, u.followers_count))

    second_hop_friends = get_hop(api, first_hop_friends)
    third_hop_friends = get_hop(api, second_hop_friends)
    fourth_hop_friends = get_hop(api, third_hop_friends)
    fifth_hop_friends = get_hop(api, fourth_hop_friends)
    all_hop_friends = []
    all_hop_friends.extend(second_hop_friends)
    all_hop_friends.extend(third_hop_friends)
    all_hop_friends.extend(fourth_hop_friends)
    all_hop_friends.extend(fifth_hop_friends)

    # 4. b) sort based on friends_count, top 10 most users following the most people
    print("Top 10 users, with a maximum of 5 friends separation from UD, who follow the most people")
    all_hop_friends.sort(key=lambda tup: tup[2], reverse=True)
    for x in range(0, 10):
        print(str(all_hop_friends[x][1]) + " with " + str(all_hop_friends[x][2]) + " follows")
    print("************************************************************************")

    # 4. c) sort based on follower_count, top 10 most followed users
    print("Top 10 users, with a maximum of 5 friends separation from UD, who have the most followers")
    all_hop_friends.sort(key=lambda tup: tup[3], reverse=True)
    for x in range(0, 10):
        print(str(all_hop_friends[x][1]) + " with " + str(all_hop_friends[x][3]) + " followers")


def get_followers(api, screen_name_to_search):
    followers = []
    followers_cursor = tweepy.Cursor(api.followers_ids, screen_name=screen_name_to_search).pages()
    keep_going = True
    while keep_going:
        try:
            followers.extend(followers_cursor.next())
            print("Paged one " + str(screen_name_to_search) + " follower cursor")
        except StopIteration:
            keep_going = False
            print("*** Retrieved all " + str(screen_name_to_search) + " followers ***")
    return followers


def get_following(api, screen_name_to_search):
    following = []
    following_cursor = tweepy.Cursor(api.friends_ids, screen_name=screen_name_to_search).pages()
    keep_going = True
    while keep_going:
        try:
            following.extend(following_cursor.next())
            print("Paged one " + str(screen_name_to_search) + " following cursor")
        except StopIteration:
            keep_going = False
            print("*** Retrieved all users " + str(screen_name_to_search) + " is following ***")
    return following


def get_friends(followers, following):
    friends = []
    for f in followers:
        if f in following:
            friends.append(f)
    return friends


def get_hop(api, previous_hop_friends):
    current_hop_ids = []
    for previous_hop_friend in previous_hop_friends:
        followers = get_followers(api, previous_hop_friend[1])
        following = get_following(api, previous_hop_friend[1])
        current_hop_ids.extend(get_friends(followers, following))
    current_hop_friends = []  # list of tuples (id, screen_name, following_count, follower_count)
    for uid in current_hop_ids:
        u = api.get_user(uid)
        current_hop_friends.append((uid, u.screen_name, u.friends_count, u.followers_count))
    return current_hop_friends


def main():
    print("************************************************************************")
    print("************************************************************************")
    print("Now printing user info...")
    print("************************************************************************")
    print("************************************************************************")
    part_one()
    print("************************************************************************")
    print("************************************************************************")
    print("Now printing the first 50 Tweets containing \"Ohio\" and \"weather\"...")
    print("************************************************************************")
    print("************************************************************************")
    part_two()
    print("************************************************************************")
    print("************************************************************************")
    print("Now printing first 5 hop friends of UD...")
    print("************************************************************************")
    print("************************************************************************")
    part_three()


if __name__ == "__main__":
    main()
